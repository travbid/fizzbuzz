# FizzBuzz
My implementation of FizzBuzz in C++.

## Build
```bash
mkdir build
cd build
cmake ..
make
./fizzbuzz
```
